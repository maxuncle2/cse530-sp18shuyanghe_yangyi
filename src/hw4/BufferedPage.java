package hw4;

import java.util.ArrayList;

import hw1.Catalog;
import hw1.Database;
import hw1.HeapFile;
import hw1.HeapPage;

public class BufferedPage {
	private HeapPage page;
	private boolean dirty = false;
	private ArrayList<Integer> readLockHolders;
	private Integer writeLockHolder = null;

	public BufferedPage(HeapPage page) {
		this.page = page;
		readLockHolders = new ArrayList<>();
	}

	public boolean addLock(int transactionId, Permissions p) {
		if (writeLockHolder != null) {
			return writeLockHolder == transactionId;
		}
		if (p == Permissions.READ_ONLY) {
			if (!readLockHolders.contains(transactionId)) {
				readLockHolders.add(transactionId);
			}
			return true;
		} else if (p == Permissions.READ_WRITE) {
			if (readLockHolders.size() > 1) {
				return false;
			}
			if (readLockHolders.size() == 1) {
				if (readLockHolders.get(0) == transactionId) {
					readLockHolders.remove(0);
				} else {
					return false;
				}
			}
			writeLockHolder = transactionId;
			return true;
		} else {
			System.out.println("Error: Unknown permission type when adding lock!");
			return false;
		}
	}

	public HeapPage getHeapPage() {
		return page;
	}

	public boolean isDirty() {
		return dirty;
	}

	public void setDirty(boolean dirty) {
		this.dirty = dirty;
	}

	public void resetPage() {
		Catalog c = Database.getCatalog();
		HeapFile hf = c.getDbFile(page.getTableId());
		page = hf.readPage(page.getId());
		dirty = false;
	}

	public void releaseTransactionLocks(int transactionId) {
		if (writeLockHolder == transactionId) {
			writeLockHolder = null;
		}
		if (readLockHolders.contains(transactionId)) {
			readLockHolders.remove(transactionId);
		}
	}

	public boolean hasLockBy(int transactionId) {
		return writeLockHolder == transactionId || readLockHolders.contains(transactionId);
	}

	public void updatePage(HeapPage newPage) {
		this.page = newPage;
	}
}
