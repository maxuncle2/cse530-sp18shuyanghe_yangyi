package hw4;

import java.io.IOException;
import java.util.HashMap;

import hw1.Catalog;
import hw1.Database;
import hw1.HeapFile;
import hw1.HeapPage;
import hw1.Tuple;

/**
 * BufferPool manages the reading and writing of pages into memory from disk.
 * Access methods call into it to retrieve pages, and it fetches pages from the
 * appropriate location.
 * <p>
 * The BufferPool is also responsible for locking; when a transaction fetches a
 * page, BufferPool which check that the transaction has the appropriate locks
 * to read/write the page.
 */
public class BufferPool {
	/** Bytes per page, including header. */
	public static final int PAGE_SIZE = 4096;

	/**
	 * Default number of pages passed to the constructor. This is used by other
	 * classes. BufferPool should use the numPages argument to the constructor
	 * instead.
	 */
	public static final int DEFAULT_PAGES = 50;

	private static final int DEADLOCK_TIME = 5000;
	private static final int DEADLOCK_INTERVAL = 1000;

	int maxPages;
	HashMap<Integer, BufferedPage> bufferedPages;

	/**
	 * Creates a BufferPool that caches up to numPages pages.
	 *
	 * @param numPages
	 *            maximum number of pages in this buffer pool.
	 */
	public BufferPool(int numPages) {
		maxPages = numPages;
		bufferedPages = new HashMap<Integer, BufferedPage>();
	}

	HeapFile getHeapFile(int tableId) {
		Catalog c = Database.getCatalog();
		HeapFile hf = c.getDbFile(tableId);
		return hf;
	}

	/**
	 * Retrieve the specified page with the associated permissions. Will acquire a
	 * lock and may block if that lock is held by another transaction.
	 * <p>
	 * The retrieved page should be looked up in the buffer pool. If it is present,
	 * it should be returned. If it is not present, it should be added to the buffer
	 * pool and returned. If there is insufficient space in the buffer pool, an page
	 * should be evicted and the new page should be added in its place.
	 *
	 * @param tid
	 *            the ID of the transaction requesting the page
	 * @param tableId
	 *            the ID of the table with the requested page
	 * @param pid
	 *            the ID of the requested page
	 * @param perm
	 *            the requested permissions on the page
	 */
	public HeapPage getPage(int transactionId, int tableId, int pid, Permissions perm) throws Exception {
		BufferedPage requestedPage;
		if (bufferedPages.containsKey(pid)) {
			requestedPage = bufferedPages.get(pid);
		} else {
			HeapPage pageFromFile = getHeapFile(tableId).readPage(pid);
			requestedPage = new BufferedPage(pageFromFile);
		}
		boolean lockAcquired = requestedPage.addLock(transactionId, perm);
		for (int i = DEADLOCK_TIME / DEADLOCK_INTERVAL; !lockAcquired; i--) {
			if (i <= 0) {
				// assume deadlocked
				return null;
			}
			System.out.println("sleeping");
			Thread.sleep(DEADLOCK_INTERVAL);
			lockAcquired = requestedPage.addLock(transactionId, perm);
		}
		if (bufferedPages.size() == maxPages) {
			evictPage();
		}
		bufferedPages.put(pid, requestedPage);
		return requestedPage.getHeapPage();
	}

	/**
	 * Releases the lock on a page. Calling this is very risky, and may result in
	 * wrong behavior. Think hard about who needs to call this and why, and why they
	 * can run the risk of calling it.
	 *
	 * @param tid
	 *            the ID of the transaction requesting the unlock
	 * @param tableID
	 *            the ID of the table containing the page to unlock
	 * @param pid
	 *            the ID of the page to unlock
	 */
	public void releasePage(int tid, int tableId, int pid) {
		bufferedPages.get(pid).releaseTransactionLocks(tid);
	}

	/** Return true if the specified transaction has a lock on the specified page */
	public boolean holdsLock(int tid, int tableId, int pid) {
		if (!bufferedPages.containsKey(pid)) {
			return false;
		}
		return bufferedPages.get(pid).hasLockBy(tid);
	}

	/**
	 * Commit or abort a given transaction; release all locks associated to the
	 * transaction. If the transaction wishes to commit, write
	 *
	 * @param tid
	 *            the ID of the transaction requesting the unlock
	 * @param commit
	 *            a flag indicating whether we should commit or abort
	 */
	public void transactionComplete(int tid, boolean commit) throws IOException {
		for (int pid : bufferedPages.keySet()) {
			BufferedPage bufferedPage = bufferedPages.get(pid);
			int tableId = bufferedPage.getHeapPage().getTableId();
			if (holdsLock(tid, tableId, pid) && bufferedPage.isDirty()) {
				if (commit) {
					flushPage(tableId, pid);
					bufferedPage.setDirty(false);
				} else {
					bufferedPage.resetPage();
				}
			}
			releasePage(tid, tableId, pid);
		}
	}

	/**
	 * Add a tuple to the specified table behalf of transaction tid. Will acquire a
	 * write lock on the page the tuple is added to. May block if the lock cannot be
	 * acquired.
	 * 
	 * Marks any pages that were dirtied by the operation as dirty
	 *
	 * @param tid
	 *            the transaction adding the tuple
	 * @param tableId
	 *            the table to add the tuple to
	 * @param t
	 *            the tuple to add
	 */
	public void insertTuple(int tid, int tableId, Tuple t) throws Exception {
		HeapFile heapFile = getHeapFile(tableId);
		HeapPage modifiedPage = heapFile.addTupleWithoutWriting(t);
		int pid = modifiedPage.getId();
		if (getPage(tid, tableId, pid, Permissions.READ_WRITE) == null) {
			transactionComplete(tid, false);
		} else {
			bufferedPages.get(pid).updatePage(modifiedPage);
			bufferedPages.get(pid).setDirty(true);
		}
	}

	/**
	 * Remove the specified tuple from the buffer pool. Will acquire a write lock on
	 * the page the tuple is removed from. May block if the lock cannot be acquired.
	 *
	 * Marks any pages that were dirtied by the operation as dirty.
	 *
	 * @param tid
	 *            the transaction adding the tuple.
	 * @param tableId
	 *            the ID of the table that contains the tuple to be deleted
	 * @param t
	 *            the tuple to add
	 */
	public void deleteTuple(int tid, int tableId, Tuple t) throws Exception {
		HeapFile heapFile = getHeapFile(tableId);
		HeapPage modifiedPage = heapFile.deleteTupleWithoutWriting(t);
		int pid = modifiedPage.getId();
		if (getPage(tid, tableId, pid, Permissions.READ_WRITE) == null) {
			transactionComplete(tid, false);
		} else {
			bufferedPages.get(pid).updatePage(modifiedPage);
			bufferedPages.get(pid).setDirty(true);
		}
	}

	private synchronized void flushPage(int tableId, int pid) throws IOException {
		Catalog c = Database.getCatalog();
		HeapFile hf = c.getDbFile(tableId);
		hf.writePage(bufferedPages.get(pid).getHeapPage());
	}

	/**
	 * Discards a page from the buffer pool. Flushes the page to disk to ensure
	 * dirty pages are updated on disk.
	 */
	private synchronized void evictPage() throws Exception {
		for (int pid : bufferedPages.keySet()) {
			if (!bufferedPages.get(pid).isDirty()) {
				bufferedPages.remove(pid);
				return;
			}
		}
		throw new Exception();
	}

}
